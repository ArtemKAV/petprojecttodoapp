# PetProjectToDoApp



## Setup
The first thing to do is to clone the repository:
```
$ git clone https://gitlab.com/ArtemKAV/petprojecttodoapp.git
$ cd petprojecttodoapp
```

Then you need to create file named ".env" in petprojecttodoapp directory, with such variables

```
DEBUG=True
SECRET_KEY=django-insecure-632zu2h26+!ac*uo0h47_3vhysfkzix!u-x32nr4adf9oex(i+
POSTGRES_DB=postgres
POSTGRES_NAME=postgres
POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
DJANGO_SETTINGS_MODULE=todoapp.settings
CELERY_BROKER_URL = redis://redis:6379
CELERY_RESULT_BACKEND = redis://redis:6379

# Email
#dev
EMAIL_BACKEND= django.core.mail.backends.console.EmailBackend

#prod
#EMAIL_BACKEND = django.core.mail.backends.smtp.EmailBackend


EMAIL_HOST=smtp.gmail.com
EMAIL_PORT=587
EMAIL_USE_TLS=True
EMAIL_HOST_USER= 
EMAIL_HOST_PASSWORD=

```
On your computer must be installed and launched Docker
Then run docker-compose command
```
$ docker-compose up
```

Run other terminal window and run command to migrate django

```
$ docker ps
```
Find image with '_web' in name. Copy CONTAINER ID  of this image and run command:

```
$ docker exec -it <CONTAINER ID> bash
$ python manage.py migrate
```
Also you can create superuser in this terminal window.

```
$ python manage.py createsuperuser
$ exit
```
And navigate to localhost:8000.

If the project did not start the first time, repeat the command

```
$ docker-compose up
```
