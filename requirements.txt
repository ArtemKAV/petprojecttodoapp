Django>=3.0,<4.0
psycopg2>=2.8
celery==5.2.7
redis==4.3.4
djangorestframework==3.13.1
django-filter==22.1

