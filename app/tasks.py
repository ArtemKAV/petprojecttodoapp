from django.conf.global_settings import EMAIL_HOST_USER
from django.contrib.auth import get_user_model
from datetime import datetime, date, timedelta
from django.core.mail import send_mass_mail
from celery import shared_task

User = get_user_model()


@shared_task
def send_email_every_day():
    # (subject, message, from_email, recipient_list)
    data_for_mail = []
    for user in User.objects.all():
        message = f'Hello, {user.first_name.capitalize()}!'
        tasks = user.tasks.filter(deadline_date=date.today() + timedelta(days=1))
        if tasks.count():
            message += ' Tomorrow is the deadline for those tasks:\n'
            for task in tasks:
                message += f'\t{task.name}: {task.short_text}. \n'
        message += 'We wish you to have time to do everything on time!'

        data_for_mail.append(('To Do App remind you.', message, EMAIL_HOST_USER, [user.email]))

    data_for_mail = tuple(data_for_mail)
    send_mass_mail(datatuple=data_for_mail)


@shared_task
def send_email_every_saturday():
    data_for_mail = []
    for user in User.objects.all():
        message = f'Hello, {user.first_name.capitalize()}!'
        end_date = datetime.now()
        start_date = end_date - timedelta(days=7)
        tasks = user.tasks.filter(finished_date__range=[start_date, end_date])
        if tasks.count():
            message += ' This week you completed such tasks :\n'
            for task in tasks:
                message += f'\t{task.name}: {task.short_text}. \n'

        data_for_mail.append(('Your finished tasks for this week', message, EMAIL_HOST_USER, [user.email]))

    data_for_mail = tuple(data_for_mail)
    send_mass_mail(datatuple=data_for_mail)
